import React from "react";
import Header from "../components/General/Header";
import OtherProfileContent from "../components/OtherProfile/OtherProfileContent";

const OtherProfile = () => {
  return (
    <div>
      <Header />
      <OtherProfileContent />
    </div>
  );
};

export default OtherProfile;
