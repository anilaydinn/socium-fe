import React from "react";
import Header from "../components/General/Header";
import ProfileEditBox from "../components/ProfileEdit/ProfileEditBox";

const ProfileEdit = () => {
  return (
    <div>
      <Header />
      <ProfileEditBox />
    </div>
  );
};

export default ProfileEdit;
