import React from "react";
import Header from "../components/General/Header";
import FriendList from "../components/ProfileFriends/FriendList";

const ProfileFriends = () => {
  return (
    <div>
      <Header />
      <FriendList />
    </div>
  );
};

export default ProfileFriends;
