import React from "react";
import FriendRequestList from "../components/FriendRequests/FriendRequestList";
import Header from "../components/General/Header";

const FriendRequests = () => {
  return (
    <div>
      <Header />
      <FriendRequestList />
    </div>
  );
};
export default FriendRequests;
